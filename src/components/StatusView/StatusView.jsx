import React from 'react';

const StatusView = props => {
	<output>{props.statuses.map(status => <Status key={status.id} {...status} />)}</output>;
};

export default StatusView;
