import React, { Component } from 'react';

class StatusForm extends Component {
	state = {
		status: ''
	};

	render() {
		return (
			<form>
				<input
					type="text"
					placeholder="What's on your mind?"
					value={this.state.status}
					name="status"
					onChange={e => this.setState({ status: e.target.value })}
				/>
			</form>
		);
	}
}

export default StatusForm;
