import React from 'react';

const Header = () => {
	return (
		<header>
			<h1>Wall of Status</h1>
		</header>
	);
};

export default Header;
