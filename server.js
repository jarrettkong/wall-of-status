const express = require('express');
const app = express();
const { join } = require('path');

app.use(express.json());
app.use('/', express.static(join(__dirname, 'build')));

app.listen(3001, console.log('Listeing on port 3000.'));
